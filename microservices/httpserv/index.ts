import app from './app';

const PORT = 8080;

app.listen(PORT, () => {
  console.log(`Httpserv listening at http://localhost:${PORT}`);
});