"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const express = require("express");
const fileName = '/log/logfile.txt';
const app = express();
app.get('/', (req, res) => {
    try {
        const data = fs_1.readFileSync(fileName, 'utf8');
        res.send(data);
    }
    catch (error) {
        res.send('RabbitMQ is not ready yet, please wait for a few seconds.\n');
    }
});
exports.default = app;
