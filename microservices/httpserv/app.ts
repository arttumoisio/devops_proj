import { readFileSync } from 'fs';
import express = require('express');

const fileName = '/log/logfile.txt';

const app = express();

app.get('/', (req, res) => {
    try {
        const data = readFileSync(fileName, 'utf8');
        res.send(data);
    } catch (error) {
        res.send('RabbitMQ is not ready yet, please wait for a few seconds.\n');
    }
});

export default app;
