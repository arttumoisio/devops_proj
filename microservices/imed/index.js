// const amqp = require('amqplib/callback_api');
// eslint-disable-next-line import/extensions
import { connect } from 'amqplib/callback_api.js';

setTimeout(() => {}, 12000);

connect('amqp://rabbitmq/', (error0, connection) => {
    if (error0) {
        throw error0;
    }
    connection.createChannel((error1, channel) => {
        if (error1) {
            throw error1;
        }

        const queue = 'my.o';
        const queue2 = 'my.i';

        channel.assertQueue(queue, {
            durable: false,
        });
        channel.assertQueue(queue2, {
            durable: false,
        });

        console.log(' [*] Waiting for messages in %s. To exit press CTRL+C', queue);

        channel.consume(queue, (msg) => {
            const content = msg.content.toString();

            console.log(' [x] Received %s', content);

            setTimeout(() => {
                const msg2 = `Got ${content}`;
                channel.sendToQueue(queue2, Buffer.from(msg2));

                console.log(' [x] Sent %s', msg2);
            }, 500);
        }, {
            noAck: true,
        });
    });
});
