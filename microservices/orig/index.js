import { connect } from 'amqplib/callback_api.js';

setTimeout(() => {}, 12000);

connect('amqp://rabbitmq/', (error0, connection) => {
    console.log('connected to amqp');
    if (error0) {
        throw error0;
    }
    connection.createChannel((error1, channel) => {
        if (error1) {
            throw error1;
        }

        const queue = 'my.o';

        channel.assertQueue(queue, {
            durable: false,
        });
        let n = 1;
        setInterval(() => {
            const msg = `MSG_${(n % 3) + 1}`;
            channel.sendToQueue(queue, Buffer.from(msg));
            n += 1;
            console.log(' [x] Sent %s', msg);
        }, 1500);
    });
});
