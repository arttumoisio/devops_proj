import { connect } from 'amqplib/callback_api.js';
import { unlinkSync, writeFile } from 'fs';

const path = '/log/logfile.txt';

try {
    unlinkSync(path, (err) => {
        if (err) {
            console.error(err);
        }
        console.log('logfile was deleted');
    });
} catch (error) {
    console.error(error);
}

setTimeout(() => {}, 12000);

const writeToFile = (queue, content) => {
    const date = new Date();
    const logstring = `${date.toISOString()} Topic ${queue}: ${content}\n`;

    writeFile(path, logstring, { flag: 'as' }, (err) => {
        if (err) throw err;
        console.log('Wrote', logstring);
    });
};

connect('amqp://rabbitmq/', (error0, connection) => {
    if (error0) {
        throw error0;
    }
    connection.createChannel((error1, channel) => {
        if (error1) {
            throw error1;
        }

        const queue = 'my.o';
        const queue2 = 'my.i';

        channel.assertQueue(queue, {
            durable: false,
        });
        channel.assertQueue(queue2, {
            durable: false,
        });

        console.log(` [*] Waiting for messages in ${queue} and ${queue2}. To exit press CTRL+C`);

        channel.consume(queue, (msg) => {
            const content = msg.content.toString();
            writeToFile(queue, content);
        }, {
            noAck: true,
        });
        channel.consume(queue2, (msg) => {
            const content = msg.content.toString();
            writeToFile(queue2, content);
        }, {
            noAck: true,
        });
    });
});
