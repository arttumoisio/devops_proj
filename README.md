## Perceived (in your mind) benefits of the topic-based communication compared to request-response (HTTP)

Topic-based communication uses the message broker to distribute the messages, so the producer doesn't have to care about the recievers. Topic-based communication doesn't have to use polling or some other kind of loop to keep the connection open and new messages are pushed to the consumers.

## Your main learnings

I learned a lot about the composition of a docker-compose.yaml file. I created separate networks for the front and back fo the application so that as few of the containers as possible could reach eachother. The containers can only reach eachother if specified. Httpserv-container is not in any network and thus can't reach any containers, it only reads the file. This practise further ensures the safety in the docker environment
