import * as request from 'supertest';
import app from '../microservices/httpserv/app';


describe("Test the root path", () => {
    test("It should response to the GET method", done => {
      request(app)
        .get("/")
        .then(response => {
          expect(response.statusCode).toBe(200);
          done();
        });
    });
  });